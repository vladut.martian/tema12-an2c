var search = new Vue({
    el: '#app',
    data: {
        colors: [],
        nrTried: [],
        positions: [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
        colorNr: 8,
        nrTrs: 0,

    },
    methods: {
        showDiv: function (pos) {
            this.nrTried.push(pos);
            document.getElementById(pos).style.backgroundColor = this.colors[pos];
            this.nrTrs += 1;

            if (this.nrTrs == 2) {
                let firstColor = document.getElementById(this.nrTried[0]).style;
                firstColor.backgroundColor;
                let secondColor = document.getElementById(this.nrTried[1]).style;
                secondColor.backgroundColor;
                setTimeout(() => {
                    if (firstColor != secondColor) {
                        firstColor.backgroundColor = "#802b00";
                        secondColor.backgroundColor = "#802b00";
                    } else {
                        firstColor.visibility = "hidden";
                        secondColor.visibility = "hidden";
                    }
                    this.nrTried = [];
                    this.nrTrs = 0;
                }, 700);
            }

        },
        shuffle: function (array) {
            array.sort(() => Math.random() - 0.5);
        },
        getColros: function () {
            for (let i = 0; i < this.colorNr; i++) {
                var color = "#" + Math.random().toString(16).slice(2, 8).toUpperCase();
                this.colors.push(color);
                this.colors.push(color);

            }

            this.shuffle(this.positions);
        },

    },
});